package serviceSpark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object SparkCT {
  val sparkSession: SparkSession = SparkSession.builder()
    .appName("APISPARK")
    .master("local[4]")
    .getOrCreate()

//  def getNewSession(): SparkSession = {
//    Logger.getLogger("org").setLevel(Level.ERROR)
//    sparkSession.newSession()
//  }
}
