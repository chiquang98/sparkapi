package serviceSpark

import org.scalatra.ScalatraServlet
import thread.FixedThreadPoolExampleScala

class WebService extends ScalatraServlet  {
  get("/") {
    "Scalatra rules!"
  }
  get("/user"){
//    val id = params("id")
    "user"
  }
  get("/user/:id"){
    val rs:List[String] = FixedThreadPoolExampleScala.getRS({params("id")})
    <p>Hello, {params("id")}</p>
    rs
  }
}