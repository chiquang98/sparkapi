package thread

import java.util.concurrent.{ExecutorService, Executors, Future}

object FixedThreadPoolExampleScala {
  val NUM_OF_THREAD = 10
  val executor: ExecutorService = Executors.newFixedThreadPool(NUM_OF_THREAD)

  def getRS(id: String): List[String] = {
    var worker = new WorkerThreadScala(id)
    var futureVar: Future[List[String]] = executor.submit(worker)
    println("finished: " + futureVar.isDone)
    val lrs = futureVar.get()
    lrs
  }
}
