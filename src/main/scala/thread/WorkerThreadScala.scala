package thread

import java.util.concurrent.Callable

import org.apache.spark.sql.functions.col
import serviceSpark.SparkCT

class WorkerThreadScala(id:String) extends Callable[List[String]]{
  var valueRS = "init"
  var listRS = List()
  def processCommand():List[String] = {
      val sparkSS = SparkCT.sparkSession
      val df = sparkSS.read.options(Map("header"->"false","delimiter"->"|")).csv("E:\\Tài liệu\\Big Data\\MultithreadSpark\\DataTest\\TestData.csv")
      df.where(col("_c0").contains(id)).toJSON.collect().toList
  }

  override def call(): List[String] = {
    val listrs = processCommand()
    listrs
  }
}
